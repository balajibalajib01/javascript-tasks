var playerScore = 0;
var computerScore = 0;
var moves = 0;
var playersChoice=document.querySelector('.playerChoice');
var computersChoice=document.querySelector('.computerChoice');

var playGame= function(){
    var rockBtn = document.querySelector('.rock');
    var paperBtn = document.querySelector('.paper');
    var scissorBtn = document.querySelector('.scissor');
    
    var playerOptions = [rockBtn,paperBtn,scissorBtn];
    var computerOptions = ['Rock','Paper','Scissors']
    
    playerOptions.forEach(option => {
        option.addEventListener('click',function(){

            var movesLeft = document.querySelector('.movesleft');
            moves++;
            movesLeft.textContent = `Moves Left: ${10-moves}`;
            
            var choiceNumber = Math.floor(Math.random()*3);
            var computerChoice = computerOptions[choiceNumber];

            console.log(option);	
            playersChoice.textContent=option.value;

            playersChoice.style.color='black';
            computersChoice.textContent=computerChoice;
            computersChoice.style.color='black	';

            winner(this.textContent,computerChoice)
            
            if(moves == 10){
                setTimeout(gameOver(playerOptions,movesLeft),5000);
				//gameOver(playerOptions,movesLeft)
            }
        })
    })   
}

var winner = function(player,computer) {
    var result = document.querySelector('.result');
    var playerScoreBoard = document.querySelector('.p-count');
    var computerScoreBoard = document.querySelector('.c-count');
    player = player.toLowerCase();
    computer = computer.toLowerCase();
    if(player === computer){
        result.textContent = 'Match Tie'
    }
    else if(player == 'rock' && computer == 'paper'){
        result.textContent = 'Computer Won';
        computerScore++;
        computerScoreBoard.textContent = computerScore;
    }
    else if(player == 'scissors' && computer == 'rock'){
        result.textContent = 'Computer Won';
        computerScore++;
        computerScoreBoard.textContent = computerScore;
    }
    else if(player == 'paper' && computer == 'scissors'){
        result.textContent = 'Computer Won';
        computerScore++;
        computerScoreBoard.textContent = computerScore;
    }
    else{
        result.textContent = 'Player Won';
        playerScore++;
        playerScoreBoard.textContent = playerScore;
    }
}

var gameOver = function(playerOptions,movesLeft){
    var chooseMove = document.querySelector('.main');
    var result = document.querySelector('.result');
    var restartBtn = document.querySelector('.restart');
	var playerChoice=document.querySelector('.playerChoice');
	var computerChoice=document.querySelector('.computerChoice');

    playerOptions.forEach(option => {
        option.style.display = 'none';
    })

	playerChoice.style.border='none';
	playersChoice.textContent='';
	computerChoice.style.border='none';
	computersChoice.textContent='';


    chooseMove.textContent = 'Game Over!!'
    movesLeft.style.display = 'none';

    if(playerScore > computerScore){
        result.style.fontSize = '2rem';
        result.textContent = 'You Won The Game'
        result.style.color = 'green';
    }
    else if(playerScore < computerScore){
        result.style.fontSize = '2rem';
        result.textContent = 'Computer Won The Game';
        result.style.color = 'red';
    }
    else{
        result.style.fontSize = '2rem';
        result.textContent = 'Tie';
        result.style.color = 'green'
    }
    restartBtn.textContent = 'Restart';
    restartBtn.style.display = 'flex'
    restartBtn.addEventListener('click',() => {
        window.location.reload();
    })
}

playGame();