document.getElementById("form").onsubmit=function(e){
    var first_Name=nameValidate(firstName,firstNameErr);
    var last_Name=nameValidate(lastName,lastNameErr);
    var password=passwordValidate();
    var email_id=emailidValidate();
    var mobile_Number=mobileValidate();
    var _gender=genderValidate();
    var _url=urlValidate();

    if(first_Name==true && last_Name==true && password==true && email_id==true && mobile_Number==true && _gender==true && _url==true){
        return true;
    }
    else{
        return false;
    }
    
}

var firstName=document.getElementById('firstname');
var firstNameErr;
firstName.onchange=function(){
  firstNameErr=document.getElementById('firstname-error');
  // firstNameValidate();
  nameValidate(firstName,firstNameErr);
}

var lastName=document.getElementById('lastname');
var lastNameErr;
lastName.onchange=function(){
  lastNameErr=document.getElementById('lastname-error');
  // lastNameValidate();
  nameValidate(lastName,lastNameErr);
 }

var passWord=document.getElementById('pass');
passWord.onchange= function(){
  passwordValidate();
 }

var emailId=document.getElementById('e-id');
emailId.onchange=function(){
  emailidValidate();
 }

var mobileNumber=document.getElementById('mobile');
mobileNumber.onchange=function(){
  mobileValidate();
 }

var url=document.getElementById('url');
url.onchange=function(){
  urlValidate();
 }

function nameValidate(names,nameErr){
  var nameValue=names.value.trim();
  console.log(nameValue);
  var validName=/^[A-Za-z]+$/;
  
  if(nameValue==""){
    nameErr.textContent="Name should not be empty";
    return false;
  }
  else if(!validName.test(nameValue)){
    nameErr.textContent="Name must be only string without white spaces";
    return false;
  }
  else{
    nameErr.textContent="";
    return true;
  }
 }

var passwordValidate= function(){
   var passwordValue=passWord.value.trim(); 
   console.log(passwordValue);
   var validPassword=/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
   var passwordErr=document.getElementById('password-error');
   if(passwordValue=="")
   {
    passwordErr.textContent="Password is required";
    return false;
   }else if(validPassword.test(passwordValue)){
     passwordErr.textContent="Password must have atleast one Uppercase, lowercase, digit, special characters & 8 characters";
     return false;
   }
   else{
     passwordErr.textContent="";
     return true;
   }
}

var emailidValidate= function(){
   var emailAddressValue=emailId.value.trim(); 
   console.log(emailAddressValue);
   var validEmailAddress=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
   var emailAddressErr=document.getElementById('eid-error');
   if(emailAddressValue=="")
   {
    emailAddressErr.textContent="Email Address is required";
    return false;
   }else if(!validEmailAddress.test(emailAddressValue)){
     emailAddressErr.textContent="Email Addre must be in valid formate with @ symbol";
     return false;
   }else{
     emailAddressErr.textContent="";
     return true;
   }
}

var mobileValidate= function(){
   var mobileNumberValue=mobileNumber.value.trim(); 
   console.log(mobileNumberValue);
   var validMobileNumber=/^[0-9]*$/;
   var mobileNumberErr=document.getElementById('mobile-error');
   if(mobileNumberValue=="")
   {
    mobileNumberErr.textContent="Mobile Number is required";
    return false;
   }else if(!validMobileNumber.test(mobileNumberValue)){
     mobileNumberErr.textContent="Mobile Number must be a number";
     return false;
   }else if(mobileNumberValue.length!=10){
      mobileNumberErr.textContent="Mobile Number must have 10 digits";
   }
   else{
     mobileNumberErr.textContent="";
     return true;
   }  
}

var urlValidate= function(){
  var urlValue=url.value.trim();
  var urlErr=document.getElementById('url-error');
  var ValidUrl= /^(http|https)/;
  if(urlValue=="")
   {
    urlErr.textContent="Url is required";
    return false;
   }else if(!ValidUrl.test(urlValue)){
      urlErr.textContent="Url must starts with http/https";
      return false;
   }
   else{
     urlErr.textContent="";
     return true;
   }
}