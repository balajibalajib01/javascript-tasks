
// let dragged = null;

// const source = document.getElementById("draggable");
// console.log('s',source);
// source.addEventListener("dragstart", (event) => {
//   // store a ref. on the dragged elem
//   dragged = event.target;
//   console.log('d',dragged);
// });

// const target = document.getElementById("droptarget");
// console.log('t',target);
// target.addEventListener("dragover", (event) => {
//   // prevent default to allow drop
//   event.preventDefault();
// });

// target.addEventListener("drop", (event) => {
//   // prevent default action (open as link for some elements)
//   event.preventDefault();
//   // move dragged element to the selected drop target
//   if (event.target.className === "dropzone") {
//     dragged.parentNode.removeChild(dragged);
//     event.target.appendChild(dragged);
//   }
// });




const item = document.querySelector('.item');

// attach the dragstart event handler
item.addEventListener('dragstart', dragStart);

// handle the dragstart

function dragStart(e) {
  console.log('E',e);
  e.dataTransfer.setData('text/plain', e.target.id);
  // setTimeout(() => {
  // console.log('sd',e.dataTransfer.setData('text/plain', e.target.id));

  //     e.target.classList.add('hide');
  // }, 0);

}


const boxes = document.querySelectorAll('.box');

boxes.forEach(box => {
    box.addEventListener('dragenter', dragEnter)
    box.addEventListener('dragover', dragOver);
    box.addEventListener('dragleave', dragLeave);
    box.addEventListener('drop', drop);
});

function dragEnter(e) {
  e.preventDefault();
  console.log('e',e.target);
  e.target.classList.add('drag-over');
}

function dragOver(e) {
  e.preventDefault();
  e.target.classList.add('drag-over');
  console.log('do',e.target.classList.add('drag-over'));
}

function dragLeave(e) {
  console.log('aa');
}
function drop(e) {
  e.target.classList.remove('drag-over');

  // get the draggable element
  const id = e.dataTransfer.getData('text/plain');
  console.log('id',e.dataTransfer.getData('text/plain'));
  const draggable = document.getElementById(id);
  console.log('dr',draggable);

  // add it to the drop target
  e.target.appendChild(draggable);

  // display the draggable element
  // draggable.classList.remove('hide');
}