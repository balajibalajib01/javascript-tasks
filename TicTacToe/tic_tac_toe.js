var cells= document.querySelectorAll('.cell');
var turn=document.querySelector('#turn');
var restartBtn=document.querySelector('#reset');
var combinations= [[0,1,2],[3,4,5],[6,7,8],[0,4,8],[2,4,6],[0,3,6],[1,4,7],[2,5,8]];
console.log(cells.length);
var option=new Array(9).fill("");  //=["","","","","","","","",""];
//createarray(option,cells.length,"");
// option.length=cells.length;
console.log(option);
var currentPlayer='X';
var running=false;
var gameWon=false;

startGame();

function startGame(){
    cells.forEach(cells=> cells.addEventListener("click",cellClicked));
    restartBtn.addEventListener("click",restartGame);
    turn.textContent= `${currentPlayer}'s turn`;
    running=true;
}

function cellClicked(){
    var cellIndex=this.getAttribute('cellIndex');
    if(option[cellIndex]!="" || !running){
        return;
    }
    updateCell(this,cellIndex);
    //changePlayer();
    checkWinner();
}

function updateCell(cell,index){
    if(gameWon!=true){
    option[index]=currentPlayer;
    cell.textContent=currentPlayer;
    }
}

function changePlayer(){
    currentPlayer= (currentPlayer == 'X') ? "O":"X";
    turn.textContent=`${currentPlayer}'s turn`;
}

function checkWinner(){
    
    for(let i=0;i<combinations.length;i++){
        var condition=combinations[i];
        var cellA=option[condition[0]];
        var cellB=option[condition[1]];
        var cellC=option[condition[2]];

        if(cellA=="" || cellB=="" || cellC==""){
            continue;
        }
        if(cellA==cellB && cellB==cellC){
            gameWon=true;
            break;
        }
    }
    if(gameWon){
        turn.textContent= `${currentPlayer} wins...`;
        //return false;
    }
    else if(!option.includes("")){
        turn.textContent='Match Tie';
        running=false;
    }
    else{
        changePlayer();
    }
}

function restartGame(){
    //window.location.reload();
    console.log(option);
    currentPlayer='X';
    option=new Array(9).fill("");
    turn.textContent=`${currentPlayer}'s turn`;
    cells.forEach(cell=> cell.textContent="");
    running=true;
    gameWon=false;
}
