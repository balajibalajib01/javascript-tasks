let addToDoButton= document.getElementById('addToDo');
let toDoContainer= document.getElementById('toDoContainer');
let inputField= document.getElementById('inputField');

addToDoButton.addEventListener('click',function(){
    var paragraph= document.createElement('div');
    paragraph.className='paragraph-styling';
    paragraph.innerText= inputField.value;


    var edit=document.createElement('i');
    edit.className="far fa-edit";
    paragraph.append(edit);

    var btn=document.createElement('button');
    btn.textContent='X';
    btn.className="close-btn";
    paragraph.append(btn);

    toDoContainer.appendChild(paragraph);

    inputField.value="";
    paragraph.addEventListener('click',function(){
        paragraph.style.textDecoration= "line-through";
    })

    btn.onclick=function(){
        var close=this.parentElement;
        close.style.display="none";
    }
    // paragraph.addEventListener('dblclick',function(){
    //     toDoContainer.removeChild(paragraph);
    // })
})